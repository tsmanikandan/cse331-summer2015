package assignments.assignment3;

/**
 * Created by jessehartloff on 7/13/15.
 */
public class SimpleMain {

    /**
     * Test class to check the performance of Assignment3.securityScreen on a random website (not worst case).
     */
    public static void main(String[] args) {

        int n=10;
        int k=5;

        Website website = new Website(n, k);
        Assignment3.securityScreen(website, k);
        if(website.successfulLaunch()){
            System.out.println("successfully launched with no attackers after " +
                    website.getNumberOfFakeLaunches() + " fake launches");
        }else{
            System.out.println("launched with attacker(s) or never launched");
        }

    }

}
