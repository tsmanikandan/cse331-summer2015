package notes.ch5divideconquer.maximimumsubarray;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class MaximumSubarrayProblem {

    List<Integer> prices;
    private static final int INITIAL = 50;

    public MaximumSubarrayProblem(int n){

        prices = new ArrayList<>(n);
        int price = INITIAL;
        prices.add(price);
        Random random = new Random();

        for (int i = 1; i < n; i++) {
            price += random.nextInt(5) - 2; //price can be negative
            prices.add(price);
        }
    }

    @Override
    public String toString() {
        return "prices=" + prices;
    }

    public List<Integer> getPrices() {
        return prices;
    }
}
