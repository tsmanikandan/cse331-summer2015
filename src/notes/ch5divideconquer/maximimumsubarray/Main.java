package notes.ch5divideconquer.maximimumsubarray;

import javafx.util.Pair;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class Main {

    public static void main(String[] args) {

        int n=100;
        MaximumSubarrayProblem problem = new MaximumSubarrayProblem(n);

//        System.out.println(problem);

        long startTime = System.currentTimeMillis();
        Pair<Integer, Integer> trade = BruteForceTrader.computeBestTrade(problem);
        long bruteForceTime = System.currentTimeMillis() - startTime;

        startTime = System.currentTimeMillis();
        Pair<Integer, Integer> trade2 = StockTrader.computeBestTrade(problem);
        long linearTime = System.currentTimeMillis() - startTime;

        System.out.println();
        System.out.println("Brute Force: ");
        printTrade(trade, problem);

        System.out.println();
        System.out.println("Efficient: ");
        printTrade(trade2, problem);



        System.out.println();
        for (int i = 0; i < n; i++) {
            if(i == trade.getKey() || i == trade.getValue()){
                System.out.print("*");
            }
            System.out.print(problem.getPrices().get(i));
            if(i == trade.getKey() || i == trade.getValue()){
                System.out.print("*");
            }
            if(i < n-1){
                System.out.print(", ");
            }
        }


        System.out.println();
        System.out.println();
        System.out.println("brute force ran in " + bruteForceTime + "ms");
        System.out.println("Efficient ran in " + linearTime + "ms");
    }


    private static void printTrade(Pair<Integer, Integer> trade, MaximumSubarrayProblem problem){
        System.out.println("Purchase at " + trade.getKey() + " for " + problem.getPrices().get(trade.getKey()) + " per share");
        System.out.println("Sell at " + trade.getValue() + " for " + problem.getPrices().get(trade.getValue()) + " per share");
        System.out.println("Profit per share: " + (problem.getPrices().get(trade.getValue()) - problem.getPrices().get(trade.getKey())) );
    }
}


