package notes.ch6dynamicprogramming.knapsack;

import java.util.Comparator;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class Item {

    private int weight;
    private int value;

    public Item(int weight, int value){
        this.weight = weight;
        this.value = value;
    }

    @Override
    public String toString() {
        return "" + weight;
    }

    public int getWeight(){
        return weight;
    }

    public int getValue(){
        return value;
    }

    public static class WeightComparator implements Comparator<Item>{

        @Override
        public int compare(Item o1, Item o2) {
            return o1.getWeight() - o2.getWeight();
        }
    }

    public static class ValueComparator implements Comparator<Item>{

        @Override
        public int compare(Item o1, Item o2) {
            return o1.getValue() - o2.getValue();
        }
    }


}
