package notes.ch6dynamicprogramming.knapsack;

import java.util.*;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class GreedyKnapsack {

    public static Set<Item> generateSolution(KnapsackProblem problem){

        List<Item> items = new ArrayList<>(problem.getItems());
        int capacity = problem.getCapacity();

        Collections.sort(items, new Item.ValueComparator());
        Collections.reverse(items);
        Set<Item> packedItems = new HashSet<>(problem.getItems().size());

        int packedWeight = 0;
        for (Item item : items){
            if(packedWeight + item.getWeight() > capacity){
                break;
            } else{
                packedItems.add(item);
                packedWeight += item.getWeight();
            }
        }

        return packedItems;
    }

}
