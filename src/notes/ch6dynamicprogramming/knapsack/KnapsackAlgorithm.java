package notes.ch6dynamicprogramming.knapsack;

import java.util.*;

/**
 * Created by jessehartloff on 6/26/15.
 */
public class KnapsackAlgorithm {

    public static Set<Item> generateSolution(KnapsackProblem problem){

        List<Item> items = new ArrayList<>(problem.getItems());
        int capacity = problem.getCapacity();
        int n = items.size();

        Collections.sort(items, new Item.WeightComparator());
        Set<Item> packedItems = new HashSet<>(problem.getItems().size());


        int[][] optimal = new int[n+1][capacity+1];
        boolean[][] inSolution = new boolean[n+1][capacity+1];

        for (int i = 0; i < n+1; i++) {
            for (int j = 0; j < capacity+1; j++) {

                if(i==0){
                    optimal[i][j] = 0;
                    inSolution[i][j] = false;
                    continue;
                }

                Item currentItem = items.get(i-1);
                int currentWeight = currentItem.getWeight();
                int currentValue = currentItem.getValue();

                if(currentWeight > j){
                    optimal[i][j] = optimal[i-1][j];
                    inSolution[i][j] = false;
                    continue;
                }

                int totalWith = currentValue + optimal[i-1][j-currentWeight];
                int totalWithout = optimal[i-1][j];

                if(totalWith >= totalWithout){ // with
                    optimal[i][j] = totalWith;
                    inSolution[i][j] = true;
                } else { // without
                    optimal[i][j] = totalWithout;
                    inSolution[i][j] = false;
                }

            }
        }

        //compute packed set
        int remainingWeight = capacity;
        for(int currentIndex = n-1; currentIndex >= 0; currentIndex--){
            if(inSolution[currentIndex+1][remainingWeight]){
                packedItems.add(items.get(currentIndex));
                remainingWeight -= items.get(currentIndex).getWeight();
            }
        }

//        System.out.println("hhh: " + optimal[n][capacity]);

        return packedItems;
    }

}
