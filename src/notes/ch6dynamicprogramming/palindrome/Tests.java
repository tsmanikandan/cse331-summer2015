package notes.ch6dynamicprogramming.palindrome;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by jessehartloff on 7/23/15.
 */
public class Tests {


    @Test
    public void computeCostTest2(){
        PalindromeProblem problem = new PalindromeProblem(0);
        int[] palindrome = new int[] {5,6,5};

        int computed = problem.solutionValue(palindrome);
        int expected = 11;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed);
    }

    @Test
    public void computeCostTest22(){
        PalindromeProblem problem = new PalindromeProblem(0);
        int[] palindrome = new int[] {5,6,5, 7};

        int computed = problem.solutionValue(palindrome);
        int expected = -1;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed);
    }

    @Test
    public void computeCostTest222(){
        PalindromeProblem problem = new PalindromeProblem(0);
        int[] palindrome = new int[] {1};

        int computed = problem.solutionValue(palindrome);
        int expected = 1;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed);
    }

    @Test
    public void computeCostTest2222(){
        PalindromeProblem problem = new PalindromeProblem(0);
        int[] palindrome = new int[] {25,5,8,8,5,25};

        int computed = problem.solutionValue(palindrome);
        int expected = 38;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed);
    }

    @Test
    public void computeCostTest22222(){
        PalindromeProblem problem = new PalindromeProblem(0);
        int[] palindrome = new int[] {6,25,5,8,8,5,25};

        int computed = problem.solutionValue(palindrome);
        int expected = -1;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed);
    }

    @Test
    public void computeCostTest222222(){
        PalindromeProblem problem = new PalindromeProblem(0);
        int[] palindrome = new int[] {};

        int computed = problem.solutionValue(palindrome);
        int expected = 0;

        TestCase.assertTrue("\nExpected: " + expected + "\nComputed: " + computed, expected == computed);
    }


}
