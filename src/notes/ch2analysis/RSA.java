package notes.ch2analysis;

import java.math.BigInteger;
import java.util.Random;

/**
 * Created by jessehartloff on 5/29/15.
 */
public class RSA {

    //TODO: show different runtimes of cracking RSA and how a more efficient algo can crack more bits

    public static void go(int numberOfBits) {

        BigInteger p = BigInteger.probablePrime(numberOfBits/2, new Random());
        BigInteger q = BigInteger.probablePrime(numberOfBits/2, new Random());

        System.out.println(p);
        System.out.println(q);

        System.out.println();

        BigInteger n = p.multiply(q);

        System.out.println(n);

        factor(n);
    }

    private static void factor(BigInteger n) {

        for (BigInteger i = BigInteger.valueOf(2L); i.compareTo(n) < 0; i = i.add(BigInteger.ONE)) {

            if(n.remainder(i).compareTo(BigInteger.ZERO) == 0){
                BigInteger foundA = i;
                BigInteger foundB = n.divide(i);

                System.out.println();
                System.out.println("factored primes: " + foundA + ", " + foundB);
                System.out.println();
                return;
            }
        }

    }

    public static void main(String[] args) {
        int numberOfBits = 20;
        go(numberOfBits);
    }

}
