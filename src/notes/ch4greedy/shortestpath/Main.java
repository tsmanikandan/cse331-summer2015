package notes.ch4greedy.shortestpath;

/**
 * Created by jessehartloff on 6/24/15.
 */
public class Main {

    public static void main(String[] args) {

        int n=10;
        ShortestPathProblem problem = new ShortestPathProblem(n);

        System.out.println(problem.getGraph());
        System.out.println();

        System.out.println("start node: " + problem.getStartingNode().getID());
        System.out.println();


        long startTime;





        startTime = System.currentTimeMillis();
        int[] distances = DijkstrasAlgorithmSlow.computeSolution(problem);
        long slowTime = System.currentTimeMillis() - startTime;

        System.out.println();
        System.out.println();
        DijkstrasAlgorithm.printDistances(distances);

        System.out.println();
        System.out.println();



        startTime = System.currentTimeMillis();
        int[] distances2 = DijkstrasAlgorithm.computeSolution(problem);
        long fastTime = System.currentTimeMillis() - startTime;


        System.out.println();
        System.out.println();
        DijkstrasAlgorithm.printDistances(distances2);

        System.out.println();
        System.out.println("DijkstrasAlgorithmSlow took: " + slowTime + "ms");
        System.out.println("DijkstrasAlgorithm took: " + fastTime + "ms");

    }
}
